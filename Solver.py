#!/usr/bin/env python
from __future__ import division
from dolfin import *
from block import *
from block.iterative import LGMRES, BiCGStab, TFQMR, ConjGrad, MinRes
from precond import *
from problem import *
from result import Result
import time
import numpy
set_log_level(30)

class Solver():
   def solve(self, problem, mesh=UnitSquareMesh(10,10), itera='BiCGStab', 
             preconditioner = P_eps_NS(), 
             fg = None, velements='CG', pelements='CG',
             mu = 1, dt = 1, Nt = 1, N = None, 
             viz=False, maxiter=400,
             init_vel=1, debug=False, save_mov=False,
             stokes=False):
      """ Solves Nt timesteps of the N-S equations. """
     
      exec('iterative_solver='+itera)

      if problem.mov_name != None:
         mov_name = problem.mov_name
      else:
         mov_name = ''

      rho      = problem.rho
      rho_inv  = 1/rho
      nu       = mu/rho
      dim      = len(fg[0])
      
      if Nt in range(0,101): save_frec = 5
      elif Nt in range(101,1001): save_frec = 10
      elif Nt in range(1001,2001): save_frec = 25
      elif Nt in range(2001,5001): save_frec = 50
      elif Nt in range(5001,10001): save_frec = 100
      
      V = VectorFunctionSpace(mesh, velements, problem.vorder)
      Q = FunctionSpace(mesh, pelements, problem.porder)
      f = Expression(fg[0])
      g = Expression(fg[1]) 

      v, u = TestFunction(V), TrialFunction(V)
      q, p = TestFunction(Q), TrialFunction(Q)
      u_1  = Function(V)
      p_1  = Function(Q)
    
      if problem.get_name() in ('bif', 'dog'):
         N = "rf%d" % problem.rf

      # Generate forms
      a11 = ((1/dt)*dot(u, v) + nu*inner(grad(u), grad(v)))*dx 
      a12 = div(v)*p*dx
      a21 = div(u)*q*dx
      L1  = inner(f, v)*dx + (1/dt)*inner(u_1, v)*dx
      L2  = inner(g, q)*dx
            
      if not stokes:
         a11 += inner(dot(u_1, nabla_grad(u)),v)*dx

      # Calculate steady Stokes solution to use as initial condition
      stokesmaxiter=10 
      t = 0
      A_s  = nu*inner(grad(u), grad(v))*dx
      B_s  = div(v)*p*dx
      C_s  = div(u)*q*dx
      D_s  = Constant(0)*p*q*dx

      AA_s = block_assemble([[A_s, B_s],[C_s, D_s]])
      bb_s = block_assemble([L1, L2])
      bcs  = problem.get_bcs(mesh, V=V, Q=Q)
      bcu  = problem.get_bcu(mesh, V=V, Q=Q)
      bcp  = problem.get_bcp(mesh, V=V, Q=Q)
      problem.update_bcs(t)
      
      bb_s[1] *= rho
      bcs.apply(AA_s).apply(bb_s)
      bb_s[1] *= rho_inv

      pp  = Y_2()
      ppp = pp.generate_prec(AA_s,p,q,u,v,problem, bcu=bcu, bcp=bcp, u_1=u_1, dim=len(fg[0]))
      
      AA_sinv = LGMRES(AA_s, maxiter=stokesmaxiter, precond=ppp,show=1)
      
      up_       = AA_sinv*bb_s
      u_, p_    = up_
      p_       *= -rho
      init_its  = AA_sinv.iterations
      u_1.assign(Function(V, u_))
      p_1.assign(Function(Q, p_))

      if save_mov:
         uname = 'ut/u_%s%s.xdmf'%(str(preconditioner), mov_name)
         pname = 'ut/p_%s%s.xdmf'%(str(preconditioner), mov_name)
         ufile = File(uname)
         pfile = File(pname)
      
      # Simulate Nt timesteps, calculating average number of
      # iterations (and std. dev.) and average time spent.
      different_its = numpy.zeros(Nt)
      its   = 0
      tick  = time.time()
      t     = 0.0
      prec_build_times = numpy.zeros(Nt)
      solve_times      = numpy.zeros(Nt)
      for i in range(Nt):
         loop_timer = time.time()

         # Update bcs
         bcs = problem.get_bcs(mesh, V=V, Q=Q)
         problem.update_bcs(t)
         
         # Assemble matrices
         AA  = block_assemble([[a11,a12],
                               [a21,  0]])
         bb  = block_assemble([L1, L2])
         
         bb[1] *= rho
         bcs.apply(AA).apply(bb)
         bb[1] *= rho_inv

         # Generate preconditioner
         prec_build_start = time.time()
         prec = preconditioner.generate_prec(AA,p,q,u,v,problem, 
                  bcu=problem.get_bcu(mesh, V=V), u_1 = u_1,
                  bcp=problem.get_bcp(mesh, Q=Q), dim=dim)
         prec_build_stop = time.time()
         prec_build_times[i] = prec_build_stop - prec_build_start

         solve_start = time.time()
         p_    *= -rho_inv
         AAinv  = iterative_solver(AA, precond=prec, initial_guess=up_, \
                     tolerance=1e-14, show=1, maxiter=maxiter)
         try:
            up_  = AAinv*bb
         except: 
            print "BiCGStab breakdown"
            print str(preconditioner), str(problem), N, mu, rho, '\n'
            sys.exit()
         solve_stop = time.time()
         solve_times[i] = solve_stop - solve_start

         u_, p_ = up_
         p_    *= -rho

         u_1.assign(Function(V, u_))
         p_1.assign(Function(Q, p_))
         its += AAinv.iterations
         different_its[i] = AAinv.iterations
         loop_time_passed = time.time() - loop_timer
         
         # Reporting on each iteration
         print "%3d: solve time = %.2f, total time = %.2f, iterations = %d, time value = %.4f" \
            % (i,solve_times[i], loop_time_passed, AAinv.iterations, t)
         
         if save_mov and i % save_frec == 0:
            print "t=%.3f, saved functions to %s,%s" % (t,uname,pname)
            ufile << (u_1, t)
            pfile << (p_1, t)
         if viz:
            plot(u_1)
            #plot(p_1)
         if debug:
            plot(u_1, interactive=True)
            plot(Function(Q, p_), interactive=True)
            Nt = 1; break

         t += dt
         if AAinv.iterations == maxiter: 
            print "\nmaxiter exceeded"
            print str(preconditioner), str(problem), N, mu, rho, '\n'
            return Result()

         # Any additional tests or action to do each timestep add here:

         # --------------------------------------

      tock = (time.time()-tick)/Nt
      its /= float(Nt)
      self.stdev = sqrt(numpy.sum((different_its-its)**2)/(Nt-1)) if Nt!=1 else 0
      average_build_time = numpy.sum(prec_build_times)/Nt
      average_solve_time = numpy.sum(solve_times)/Nt

      # output handling
      r = Result()
      r.take_input(time=tock, iterations=int(its), 
                   problem=problem.get_name(),
                   velements=velements, pelements=pelements,
                   mu=mu, h=mesh.hmin(), prec = preconditioner, N=N,
                   stdev=int(self.stdev), sol=itera, dt=dt, rho=rho,
                   Nt=Nt, build_time=average_build_time, 
                   solve_time=average_solve_time)
      return r



