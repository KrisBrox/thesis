function A = assemble_mat(a1, a2, a3, a4)
   A = [[a1 a2]; [a3 a4]]; 
end

M3_b0;        
                     M4_b3;
M5_b0; M5_b1; M5_b2; M5_b3;

AA; p11; p12a; p12b; p13; p14a; p14b; p14c;
Pp_bc; Pu_bc; 

A = M5_b0; 
G = M5_b1;
D = M5_b2;
AA22 = M5_b3;
Iu = M3_b0;
Ip = M4_b3;
Lc_inv = inv(p12b);
IIp = eye(rows(Ip));
IIu = eye(rows(Iu));

if (max(max(abs(p14c-Pp_bc))) == 0)
   %printf('timestokes\n')
   Sinv = inv(p14a) + inv(p14b) - Pp_bc;
else
   %printf('oseen\n')
   Sinv = inv(p14a) + inv(p14b) + p14c - 2*Pp_bc;
end

D0 = zeros(rows(D), columns(D));
G0 = zeros(rows(G), columns(G));

% build preconditioner
P1 = assemble_mat(IIu, G*Lc_inv, D0, Sinv);
P2 = assemble_mat(IIu, G0, D, -IIp);
P3 = assemble_mat(inv(A), G0, D0, IIp);
P = P1*P2*P3;
m = rows(P);

% max abs(eig)/min abs(eig) kappa (formatted to be compatible with old code)
e = qz(P*AA,eye(rows(AA)));
c_eig_max = max(abs(sort(imag(e))))
if (c_eig_max < 1e-12)
   e = abs(e);
end
eigs = sort(e)
e = abs(e);
e = sort(e);
n = size(e)(1);

kappa2 = e(n(1)) / e(1);
if (e(1) < 1e-10)
   printf("singular system, lambda_min=%.2f\n", e(1))
   kappa2 = e(n(1))/e(2);
endif

% spectral norm kappa 2
singvals = svd(P*AA);
singvals = sort(singvals);
if (abs(singvals(1)) < 1e-12)
   kappa    = singvals(m)/singvals(2)
else
   kappa    = singvals(m)/singvals(1)
end

arglist = argv();
dt = arglist{1};
nu = arglist{2};
N  = arglist{3};
ve = arglist{4};
pe = arglist{5};

printf("n=%d N=%s dt=%s nu=%s ve=%s pe=%s cai1\n", n, N, dt(1:6), nu(1:6), ve, pe)
printf("low:  %f %f\n", e(1), e(2));
printf("high: %f %f\n", e(n(1)-1), e(n(1)));
printf("kappa2=%f\n", kappa2);


