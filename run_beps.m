function A = assemble_mat(a1, a2, a3, a4)
   A = [[a1 a2]; [a3 a4]]; 
end

M5_b0; M5_b1; M5_b2; M5_b3;

AA; p11; p12a; p12b; p13; p14a; p14b; p14c;
Pp_bc; Pu_bc;

A = M5_b0; 
G = M5_b1;
D = M5_b2;
A22 = M5_b3;

if (max(max(abs(p14c-Pp_bc))) == 0)
   %printf('timestokes\n')
   Sinv = inv(p14a) + inv(p14b) - Pp_bc;
else
   %printf('oseen\n')
   Sinv = inv(p14a) + inv(p14b) + p14c - 2*Pp_bc;
end

%S = D*inv(diag(diag(A)))*G + A22;
%Sinv = inv(S);

D0 = zeros(rows(M5_b2), columns(M5_b2));
G0 = zeros(rows(M5_b1), columns(M5_b1));

%M11 = inv(A);
%M12 = G0;
%M21 = D0;
%M22 = Sinv;

%eu = abs(eig(M11));
%ep = abs(eig(M22));
%conv_eig_min = min(abs(eig(p14c, eye(size(p14c)))));
%conv_eig_max = max(abs(eig(p14c, eye(size(p14c)))));

%singular_S = max(abs(eig(inv(Sinv)*Sinv, eye(size(Sinv)))))
%S_eig_max = max(eig(Sinv))
%printf("conv_eig_minmax %.2f %.2f\n",conv_eig_min, conv_eig_max);

%lambda_max_u = max(max(abs(eu)))
%lambda_min_u = min(min(abs(eu)))
%lambda_max_p = max(max(abs(ep)))
%lambda_min_p = min(min(abs(ep)))

% find eigenvalues
P = assemble_mat(inv(A), G0, D0, Sinv);
n = size(P)(1);

% max abs(eig)/min abs(eig) 
e = qz(P*AA,eye(rows(AA))); 
eigs = sort(e)
e = abs(e);
e = sort(e);
kappa2 = e(n) / e(1);
if (e(1) < 1e-10)
   printf("singular system, lambda_min=%.2f\n", e(1))
   kappa2 = e(n)/e(2);
endif

% spectral norm condition number
singvals = svd(P*AA);
singvals = sort(singvals);
if (abs(singvals(1)) < 1e-12)
   kappa    = singvals(n)/singvals(2)
else
   kappa    = singvals(n)/singvals(1)
end

arglist = argv();
dt = arglist{1};
nu = arglist{2};
N  = arglist{3};
ve = arglist{4};
pe = arglist{5};

printf("n=%d N=%s dt=%s nu=%s ve=%s pe=%s kent\n", n, N, dt(1:6), nu(1:6), ve, pe)
printf("low:  %f %f\n", e(1), e(2));
printf("high: %f %f\n", e(n(1)-1), e(n(1)));
printf("kappa2=%f\n", kappa2);



