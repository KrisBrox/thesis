#!/usr/bin/env python
from __future__ import division
from dolfin import * 
from block import *
from block.iterative import *
import scipy.linalg
import numpy as np
import os, sys

def dump_matrix(filename, name, AA):
   f = open(filename, 'w')
   for i in range(AA.shape[0]):
     for j in range(AA.shape[1]):
         if abs(AA[i,j]) > 1e-9 \
            or (i, j) == (AA.shape[0]-1,AA.shape[1]-1) \
            or (i, j) == (0,0): 
            f.write("%s (%d, %d) = %e;\n " % (name,i+1,j+1,AA[i,j])) 

def assemble_mat(M):
   [M11, M12, M21, M22] = M
   a = M11.shape[0]; b = M22.shape[0]
    
   M = np.zeros((a+b, a+b))
   M[:a, :a] = np.copy(M11)
   M[:a, a:] = np.copy(M12)
   M[a:, :a] = np.copy(M21)
   M[a:, a:] = np.copy(M22)
   return M

def unpack(A):
   [[a1, a2],[a3, a4]] = A
   return [a1.array(), a2.array(), a3.array(), a4.array()]

class WholeBoundary(SubDomain):
   def inside(self, x, on_boundary):
      return on_boundary

class pipeflowboundU(SubDomain):
   def inside(self, x, on_boundary):
      y = near(x[0], 0) or near(x[1], 0) or near(x[1], 1)
      return y and on_boundary

class pipeflowboundP(SubDomain): 
   def inside(self, x, on_boundary):
      return near(x[0], 1) and on_boundary

class pipeflowU(Expression):
   def value_shape(self):
      return (2,)
   def eval(self, values, x):
      inlet_u = 4*x[1]*(1-x[1])
      values[0] = inlet_u if near(x[0],0) else 0
      values[1] = 0

class DrivenCavityBoundary2D(Expression):
   def value_shape(self):
      return (2,)
   def eval(self, values, x):
      values[0] = 1 if near(x[1],1) else 0
      values[1] = 0

mu = 1;     # viscosity 
dt = 1;  # time step 
N = 6;      # refinement in both spatial directions
conv_switch = 1;    # turn convection on or off
vorder = 2  # order of velocity field polynomials
porder = 1  # order of pressure field polynomials
DG = False
CR = False
velements = 'CG'
pelements = 'CG'
name = 'cai1'
DC = False

# Parse command-line options
for s in sys.argv[1:]:
   try:
      exec(s)
   except:
      name = s.split('=')[1]

# Create mesh and function space
mesh = UnitSquareMesh(N,N)
Vh = VectorFunctionSpace(mesh, velements, vorder)
Ph = FunctionSpace(mesh, pelements, porder)
v, q = TestFunction(Vh), TestFunction(Ph)
u, p = TrialFunction(Vh), TrialFunction(Ph)
u0 = Function(Vh)

# Define the system 
a11 = (1/dt)*inner(u, v)*dx \
      + mu*inner(grad(u), grad(v))*dx \
      + Constant(conv_switch)*inner(dot(u0, nabla_grad(u)), v)*dx
a12 = div(v)*p*dx 
a21 = div(u)*q*dx       
a22 = Constant(0)*p*q*dx
b1  = (1/dt)*inner(u0, v)*dx  
b2  = Constant(0)*q*dx

# Other forms for preconditioners
W  = Constant(conv_switch)*inner(dot(u0, grad(p)), q)*dx
Kp = inner(grad(p), grad(q))*dx
Ku = inner(grad(u), grad(v))*dx
Ku_mu = mu*inner(grad(u), grad(v))*dx
D  = a21 
G  = a12
A  = (1/dt)*inner(u, v)*dx \
      + mu*inner(grad(u), grad(v))*dx \
      + Constant(conv_switch)*inner(dot(u0, nabla_grad(u)), v)*dx
Ip = p*q*dx
Iu = inner(u, v)*dx
D0 = Constant(0)*div(u)*q*dx
G0 = Constant(0)*div(v)*p*dx

# Choosing boundary conditions
bc_type_pipeflow = not DC
bc_type_drivencavity = DC
if bc_type_pipeflow:
   bcufunc = pipeflowU()
   bcu_domain = pipeflowboundU()
   print 'Velocity driven pipe flow'
   bcp = DirichletBC(Ph, Constant(0), pipeflowboundP())
   bcu = DirichletBC(Vh, bcufunc, bcu_domain)
   bcs = block_bc([bcu, bcp], True)
   
elif bc_type_drivencavity:
   print 'Driven Cavity'
   bcufunc =  DrivenCavityBoundary2D()
   bcu_domain = WholeBoundary()
   bcu = DirichletBC(Vh, bcufunc, bcu_domain)
   bcp = None
   bcs = block_bc([bcu, bcp], True)

# Calculating steady Stokes initial condition
AA_ = block_assemble([[Ku, a12],[a21, a22]])
MM_ = block_assemble([[Iu, 0],[0, p*q*dx]])
bb_ = block_assemble([b1, b2])
bcs.apply(AA_).apply(bb_)
bcs.apply(MM_)
[[A_, _],[_, _]] = AA_
[[_, _],[_, M_]] = MM_
xx = AA_.create_vec()
xx.randomize()
from block.algebraic.petsc import AMG
AAp = block_mat([[AMG(A_), 0],[0, AMG(M_)]])
AAinv_  = LGMRES(AA_,initial_guess=xx, precond=AAp, maxiter=100, tolerance=1e-14)
up_    = AAinv_*bb_
u_, p_ = up_
u0.assign(Function(Vh, u_))

# Assembling forms
p3 = block_assemble([[Iu, G0], [D, -Ip]])
p4_inv = block_assemble([[A, G0], [D0, Ip]]) 

# Hack to create P1=[[Iu**-1, G*(Kp)**-1], [0, S**-1]], 
# where S**-1 is the schur pressure complement inverse approximation
P1_1 = block_assemble([[Iu, G0], [D0, Ip]])
P1_3 = block_assemble([[Iu, G],  [D0, Ip]])
P1_4 = block_assemble([[Iu, G0], [D0, (1/mu) * Ip]])
P1_5 = block_assemble([[Iu, G0], [D0, dt  * Kp + dt*Ip]])
P1_6 = block_assemble([[Iu, G0], [D0, W]])
bcs.apply(P1_1); bcs.apply(P1_3)
bcs.apply(P1_4); bcs.apply(P1_5); bcs.apply(P1_6)
bcs.apply(p3);   bcs.apply(p4_inv);

# dt*Mu for yosida with lumped mass matrix
lump = block_assemble([[(1/dt)*Iu, G0],[D0, dt*Ip]])
bcs.apply(lump); 

# avoid singular Kp in lid-driven cavity:
if DC:
   P1_2 = block_assemble([[Iu, G0], [D0, Kp+Ip]])
else:
   P1_2 = block_assemble([[Iu, G0], [D0, Kp]]) 
bcs.apply(P1_2); 

# select the components we need
p11  = P1_1[0][0].array()
p12a = P1_3[0][1].array()
p12b = P1_2[1][1].array()
p13  = P1_1[1][0].array()
p14a = P1_4[1][1].array() 
p14b = P1_5[1][1].array() 
p14c = P1_6[1][1].array()
lumpMu = lump[0][0].array()
lumpMp = lump[1][1].array()

# switched to Laplace**-1*dot(w,grad) for the convective term,
# NB: only used if convection is turned on (see octave scripts).
p12b_inv = scipy.linalg.inv(p12b) if not DG and not CR else 1
p14c_alt = p14c*p12b_inv

# write matrices to octave format
dump_matrix('p11.m', 'p11',p11)
dump_matrix('p12a.m','p12a',p12a)
dump_matrix('p12b.m','p12b',p12b)
dump_matrix('p13.m', 'p13',p13)
dump_matrix('p14a.m','p14a',p14a)
dump_matrix('p14b.m','p14b',p14b)
dump_matrix('p14c.m','p14c',p14c_alt)
dump_matrix('lumpMu.m','lumpMu',lumpMu) 
dump_matrix('lumpMp.m','lumpMp',lumpMp) 

AA = block_assemble([[a11, a12],[a21, a22]]) 
bcs.apply(AA); 
[[a11, a12], [a21, a22]] = AA
[aa11, aa12,aa21, aa22] = [a.array() for a in [a11, a12, a21, a22]] 
AA = assemble_mat([aa11, aa12, aa21, aa22])

# Getting 1 instead 2 or 3 on the diagonal where we enforced bcs:
Iu0 = Constant(0)*inner(u,v)*dx 
Ip0 = Constant(0)*p*q*dx
P_bc = block_assemble([[Iu0, G0], [D0, Ip0]])
bcs.apply(P_bc)
Pp_bc = P_bc[1][1].array()
Pu_bc = P_bc[0][0].array()
dump_matrix('Pp_bc.m', 'Pp_bc', Pp_bc)
dump_matrix('Pu_bc.m', 'Pu_bc', Pu_bc)

# Write the rest of the matrices to octave format files
[p3, p4_inv] = [unpack(A) for A in [p3, p4_inv]]
matnum = 3
for lst in (p3, p4_inv, [aa11, aa12, aa21, aa22]):
   blocknum = 0
   for block in lst:
      dump_matrix('M%d_b%d.m' % (matnum, blocknum),\
                  'M%d_b%d' % (matnum, blocknum),block)
      blocknum += 1
   matnum += 1

# Write the problem matrix
dump_matrix('AA.m',  'AA',  AA)

# Do inverting and eigenvalue calculation in octave
os.system('octave -q run_%s.m %f %f %d %s %s' \
            % (name, dt, mu, N, velements, pelements))


# Clean the directory
os.system('rm P*.m AA.m')
os.system('rm M*_b*.m p1*.m')
#os.system('rm kpnu.m Ipdt.m Ku.m')
os.system('rm lump*.m')

