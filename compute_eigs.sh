#!/bin/bash

name='beps'    # preconditioner. alternatives: peps, y1, y2
DC='False'     # False: pipeflow, True: lid-driven cavity

mu=0.01        # Viscosity
N=8            # size of unitsquaremesh, both x and y dims

conv=1         # 0 to turn off convection



python eigenvalues.py name=$name DC=$DC conv_switch=$conv mu=$mu N=$N $@

#for name in 'beps' 'peps' 'y1' 'y2'; do
#   python eigenvalues.py name=$name DC=$DC conv_switch=$conv mu=$mu N=$N
#done


