#!/usr/bin/env python
from __future__ import division
from dolfin import *
from problem import BifurcationAneurysm
from precond import *
from Solver import Solver

# Simulation Parameters
rf       = 0           
prec     = 'P_eps'            # or B_eps, P_eps, Y_2
itera    = 'BiCGStab'         # or 'MinRes' etc.
Nt       = 100            
stokes   = False              # True -> no convection

# Physical Parameters
mu       = 0.00345                  # viscosity
dt       = 0.0005                  # timestep
rho      = 0.00106                     # Density
fg       = (('0','0','0'),('0')) # External forces

viz      = True        # flag to plot solution during run
maxiter  = 400
factor   = 1000            # Multiplier to velocity bcs

save_mov = False        # Flag to save functions in xdmf-format
mov_name = ''

velements= 'CG'         
pelements= 'CG'         
vorder   = 2
porder   = 1

import sys
for s in sys.argv[1:]:
   exec(s)               # allow parameters like 'N=10' etc.

if prec == 'B_eps':
   if stokes:
      prec = B_eps()
   else:
      prec = B_eps_NS()

elif prec == 'P_eps':
   if stokes:
      prec = P_eps()
   else:
      prec = P_eps_NS()

elif prec == 'Y_1':
   prec = Y_1()
elif prec == 'Y_2':
   prec = Y_2()

f = HDF5File(mpi_comm_world(), "model.hdf5", "r")
mesh = Mesh()
f.read(mesh, "Meshes/Mesh{}".format(rf), False)
mvc = MeshValueCollection("size_t", mesh, 2)
f.read(mvc, \
"Meshes/Mesh{}/MeshValueCollections/facet_domains".format(rf))
mf = MeshFunction("size_t", mesh, mvc)

params = {'mu':mu,'dt':dt,'Nt':Nt, 'rf':rf, 'factor':factor} 
params.update({'mov_name':mov_name, 'mesh':mesh, 'mf':mf}) 
params.update({'velements':velements, 'pelements':pelements})
params.update({'vorder':vorder, 'porder':porder, 'rho':rho})

problem  = BifurcationAneurysm()
problem.set_params(params)

solver = Solver()

r=solver.solve(problem, mesh = mesh, itera = itera, \
            preconditioner = prec, \
            velements = velements, pelements = pelements, mu = mu, \
            Nt = Nt, dt = dt, N = None, viz = viz, \
            maxiter = maxiter, fg=fg, \
            save_mov = save_mov, \
            stokes = stokes)

print "\n%s\n" % str(r)

