#!/usr/bin/env python
from dolfin import *
from block import *

class Boundary(SubDomain):
   def inside(self, x, on_boundary):
      return on_boundary

class Pipe2D(SubDomain):
   def inside(self, x, on_boundary):
      on = near(x[1], 1) or near(x[1], 0) or near(x[0], 0)     
      return on and on_boundary

class Outlet2D(SubDomain):
   def inside(self, x, on_boundary):
      return near(x[0], 1) and on_boundary

class PipeFlow2Du(Expression):
   t = 0
   factor = 1
   def value_shape(self):
      return (2,)
   def eval(self, values, x):
      inlet_u = self.factor*(0.5+0.5*cos(pi*self.t)*cos(pi*self.t))*4*x[1]*(1-x[1])
      values[0] = inlet_u if near(x[0], 0) else 0
      values[1] = 0;

class DrivenCavityBoundary2D(Expression):
   factor = 1
   t      = 0
   def value_shape(self):
      return (2,)
   def eval(self, values, x):
      s = self.factor*(0.5+0.5*cos(pi*self.t)*cos(pi*self.t))
      values[0] = s if near(x[1],1) else 0
      values[1] = 0

class problem_base():
   def __init__(self, arg=None):
      if arg!=None: self.set_params(arg)
   def user_func(self):
      return Expression('1')
   def __str__(self):
      return self.__class__.__name__
   def update_bcs(self, t=0):
      pass
   def set_params(self, args):
      self.params = args
      for key in args.iterkeys():
	      s = "self.%s=args['%s']"%(key, key)
	      exec(s)
   def add_params(self, args):
      self.params.update(args)
      for key in args.iterkeys():
         s = "self.%s=args['%s']"%(key, key)
         exec(s)
   def get_bcs(self, mesh, V):
      pass

class BifurcationAneurysm(problem_base):
   """ BCs for the artery mesh in model.hdf5
   """
   def __init__(self, *args, **kwargs):   
      problem_base.__init__(self, *args, **kwargs)
   
   def update_bcs(self, t):
      for e in self.inflow: e.set_t(float(t))
      
   def get_bcs(self, mesh, V=None, Q=None, symm_bc=False):
      bcu = self.get_bcu(mesh, V=V, Q=Q, symm_bc=symm_bc)
      bcp = self.get_bcp(mesh, V=V, Q=Q, symm_bc=symm_bc)
      return block_bc([bcu, bcp], symm_bc)
   
   def get_bcu(self, mesh, V=None, Q=None, symm_bc=False):
      indicator = 3
      c0 = 0
      if 'inflow' not in vars(self).keys():
         from cbcflow.bcs import make_poiseuille_bcs
         # cut from BifurcationAneurysm.py
         factor = 1000 if 'factor' not in vars(self) else self.factor
         profile = [0.4, 1.6, 1.4, 1.0, 0.8, 0.6, 0.55, 0.5, 0.5, 0.45, 0.4]
         profile = [pr*factor for pr in profile]
         tim = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
         self.inflow = make_poiseuille_bcs(zip(tim, profile), mesh, indicator, facet_domains=self.mf)
         for e in self.inflow: e.set_t(float(0)) 
      
      bcu_raw = [(self.inflow, indicator),
                ([c0, c0, c0], 0)]
      bcu = [DirichletBC(V.sub(d), functions[d], self.mf, region)
         for functions, region in bcu_raw
         for d in range(len(functions))]
      return bcu
      
   def get_bcp(self, mesh, V=None, Q=None, symm_bc=False):      
      bcp1 = DirichletBC(Q, Constant(0), self.mf, 1)
      bcp2 = DirichletBC(Q, Constant(0), self.mf, 2)
      bcp  = [bcp1, bcp2]
      return bcp
   
   def get_name(self):
      return 'bif'
   
   def __str__(self):
      return self.get_name()

   def get_inlet_geometry(self):
      indicator = 3
      mf = self.mf
      from cbcflow.bcs.utils import compute_boundary_geometry_acrn as geom
      area, center, radius, normal = \
            geom(self.mesh, indicator, mf)
      return area, center, radius, normal

class DrivenCavity2(problem_base):
   def __init__(self, *args, **kwargs):
      problem_base.__init__(self, *args, **kwargs)
      self.bcufunc = DrivenCavityBoundary2D()
   
   def update_bcs(self, t):
      """ allow for time-variable bcs """
      self.bcufunc.t = t

   def get_bcs(self, mesh, V, Q=None, symm_bc=False):
      self.bcufunc.factor = self.factor
      
      bcs = block_bc([DirichletBC(V, self.bcufunc,
                         Boundary()),None],symm_bc)
      return bcs

   def get_bcu(self, mesh, V=None, Q=None):
      return [DirichletBC(V, self.bcufunc, Boundary())]

   def get_bcp(self, mesh, V=None, Q=None):
      return None 

   def get_name(self):
      return 'DC'
   
   def __str__(self):
      return self.get_name()

class Pipe2(problem_base):
   def __init__(self, *args, **kwargs):
      problem_base.__init__(self, *args, **kwargs)
      self.bcpfunc = Constant(0)

   def update_bcs(self, t):
      self.bcufunc.t = t

   def get_bcs(self, mesh, V, Q, symm_bc=True):
      if 'bcufunc' not in vars(self).keys():
         self.bcufunc = PipeFlow2Du()
         self.bcufunc.factor = self.factor
      bcu = self.get_bcu(mesh, V=V, Q=Q)
      bcp = self.get_bcp(mesh, V=V, Q=Q)
      
      bcs = block_bc([bcu, bcp],symm_bc)
      return bcs

   def get_bcu(self, mesh, V=None, Q=None):
      return [DirichletBC(V, self.bcufunc, Pipe2D())]

   def get_bcp(self, mesh, V=None, Q=None):
      return [DirichletBC(Q, self.bcpfunc, Outlet2D())]

   def get_name(self):
      return 'pipe'

   def __str__(self):
      return self.get_name()

