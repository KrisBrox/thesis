#!/usr/bin/env python
from __future__ import division
from dolfin import *
from problem import Pipe2, DrivenCavity2
from precond import *
from Solver import Solver

# Simulation Parameters
N        = 16           
prec     = 'P_eps'           # or B_eps, P_eps, Y_2
itera    = 'BiCGStab'      # or 'MinRes' etc.
Nt       = 100            
stokes   = False            # True -> no convection

# Physical Parameters
mu       = 5e-3            # viscosity
dt       = 0.01            # timestep
rho      = 1               # Density
fg       = (('0','0'),('0')) # External forces

viz      = True        # flag to plot solution during run
maxiter  = 400
factor   = 1           # Multiplier to velocity bcs

save_mov = False        # Flag to save functions in xdmf-format
mov_name = ''

velements= 'CG'         
pelements= 'CG'         
vorder   = 2
porder   = 1

import sys
for s in sys.argv[1:]:
   exec(s)               # allow parameters like 'N=10' etc.

if prec == 'B_eps' and stokes:
   prec = B_eps()
elif prec == 'B_eps' and not stokes:
   prec = B_eps_NS()
elif prec == 'P_eps' and stokes:
   prec = P_eps()
elif prec == 'P_eps' and not stokes:
   prec = P_eps_NS()
elif prec == 'Y_1':
   prec = Y_1()
elif prec == 'Y_2' :
   prec = Y_2()

mesh = UnitSquareMesh(N,N)

params = {'mu':mu,'dt':dt,'Nt':Nt, 'N':N, 'factor':factor} 
params.update({'mov_name':mov_name, 'mesh':mesh}) 
params.update({'velements':velements, 'pelements':pelements})
params.update({'vorder':vorder, 'porder':porder, 'rho':rho})

problem  = Pipe2()
problem.set_params(params)

solver = Solver()

r=solver.solve(problem, mesh = mesh, itera = itera, \
            preconditioner = prec, \
            velements = velements, pelements = pelements, mu = mu, \
            Nt = Nt, dt = dt, N = N, viz = viz, \
            maxiter = maxiter, fg=fg, \
            save_mov = save_mov, \
            stokes = stokes)

print "\n%s\n" % str(r)

