#!/usr/bin/env python
from __future__ import division
from block import *
from dolfin import *
from block.algebraic.petsc import collapse, ML, ILU, InvDiag, LumpedInvDiag
from block.algebraic.petsc import AMG, SOR

class P_eps():
   def __str__(self):
      return 'P_eps'

   def generate_prec(self, AA, p, q, u, v, P, bcu=None, bcp=None,
                     u_1=None, dim=None):
      """ Input assembled forms and variables, output prec"""
      rho = P.rho; dt = P.dt; mu = P.mu; 
      nu = mu/rho
      
      [[A, G], [D, _]] = AA
      
      if bcp == None:
         Kp          = assemble(dot(grad(p), grad(q))*dx + p*q*dx) 
         Kp_dt       = assemble(dt * (dot(grad(p), grad(q))*dx + p*q*dx))
      else:
         Kp          = assemble(dot(grad(p), grad(q))*dx) 
         Kp_dt       = assemble(dt * (dot(grad(p), grad(q))*dx))
      I_p_nu      = assemble((1/nu) * p*q*dx)

      # Set bcs for the preconditioner
      if bcp != None:
         for bc in bcp:
            bc.apply(I_p_nu)
            bc.apply(Kp)
            bc.apply(Kp_dt)

      Sp = AMG(Kp_dt) + AMG(I_p_nu)
      
      P1 = block_mat([[1,       G*AMG(Kp)],
                      [0,              Sp]])
      P2 = block_mat([[1,               0],
                      [D,              -1]])
      P3 = block_mat([[AMG(A),          0], 
                      [0,               1]])
      P = P1*P2*P3
      return P

class B_eps():
   def __str__(self):
      return 'B_eps'
   def generate_prec(self, AA, p, q, u, v, P, bcu=None, bcp=None,
                     u_1=None, dim=None):
      mu = P.mu;  dt = P.dt
      rho= P.rho; nu = mu/rho
      
      [[A, B], [C, _]] = AA

      if bcp == None:
         Kp_dt    = assemble(dt*(dot(grad(p), grad(q))*dx + p*q*dx)) 
      else:
         Kp_dt    = assemble(dt*(dot(grad(p), grad(q))*dx)) 
      I_p_nu   = assemble((1/nu) * p*q*dx)
      
      # Dirichlet bcs for the pressure
      if bcp != None:
         for bc in bcp:
            bc.apply(I_p_nu)
            bc.apply(Kp_dt)

      Sp = AMG(Kp_dt) + AMG(I_p_nu)
      P  = block_mat([[AMG(A),    0],
                     [0,         Sp]])
      return P

class P_eps_NS():
   def __str__(self):
      return 'P_eps_NS'

   def generate_prec(self, AA, p, q, u, v, P, bcu=None, 
                     bcp=None, u_1=None, dim=None):
      """ Input assembled forms, test- and trial functions, output prec 
      """
      rho = P.rho; dt = P.dt; mu = P.mu; 
      nu  = mu/rho

      [[F, G], [D, _]] = AA
      if bcp == None:
         Kp    = assemble(dot(grad(p), grad(q))*dx + p*q*dx)
         Kp_dt = assemble(dt*(dot(grad(p), grad(q))*dx + p*q*dx))
      else:
         Kp    = assemble(dot(grad(p), grad(q))*dx)
         Kp_dt = assemble(dt*(dot(grad(p), grad(q))*dx))
      Mp         = assemble(p*q*dx)
      Mp_nu      = assemble((1/nu) * p*q*dx)
      Mu         = assemble(dot(u,v)*dx)
      C          = assemble(inner(dot(u_1, nabla_grad(p)), q)*dx)

      if bcp != None:
         for bc in bcp:
            bc.apply(Mp_nu)
            bc.apply(Kp)
            bc.apply(Kp_dt)
            bc.apply(C)

      Sp1 = SOR(Mp_nu) 
      Sp2 = AMG(Kp_dt) 
      Sp3 = AMG(Kp)*C
      Sp = Sp1+Sp2+Sp3    

      P1 = block_mat([[1,         G*AMG(Kp)],
                      [0,                Sp]])
      P2 = block_mat([[1,                 0],
                      [D,                -1]])
      P3 = block_mat([[AMG(F),            0], 
                      [0,                 1]])
      P = P1*P2*P3
      return P 

class B_eps_NS():
   def __str__(self):
      return 'B_eps_NS'

   def generate_prec(self, AA, p, q, u, v, P, u_1=None, 
                     bcu=None, bcp=None, dim=None):
      mu = P.mu; dt = P.dt; rho = P.rho;
      nu = mu/rho
   
      [[A, B], [C, _]] = AA

      if bcp == None:
         Kp    = assemble((dot(grad(p), grad(q))*dx + p*q*dx)) 
         Kp_dt = assemble(dt*(dot(grad(p), grad(q))*dx + p*q*dx)) 
      else:
         Kp    = assemble((dot(grad(p), grad(q))*dx)) 
         Kp_dt = assemble(dt*(dot(grad(p), grad(q))*dx)) 
      conv     = assemble(inner(dot(u_1, nabla_grad(p)), q)*dx)
      I_p_nu   = assemble((1/nu)*p*q*dx)
      
      if bcp != None:
         for bc in bcp:
            bc.apply(I_p_nu)
            bc.apply(Kp)
            bc.apply(Kp_dt)
            bc.apply(conv)

      Sp1 = SOR(I_p_nu) 
      Sp2 = AMG(Kp_dt)
      Sp3 = AMG(Kp)*conv
      Sp  = Sp1 + Sp2 + Sp3

      P   = block_mat([[AMG(A),         0],
                       [0,            Sp]])
      return P

class Y_2():
   def __str__(self):
      return 'Yosida_2'

   def generate_prec(self, AA, p, q, u, v, P, bcu=None, bcp=None, 
                     u_1=None, dim=3):
      
      [[F, G], [D, Q]]       = AA
 
      Fp = AMG(F)
      Mp = assemble(p*q*dx)
      if bcp != None:
         for bc in bcp:
            bc.apply(Mp)

      if dim == 2:
         S = D*InvDiag(F)*G + Mp
      elif dim == 3: 
         S = D*InvDiag(F)*G 

      Sp = AMG(collapse(S))
      
      p1 = block_mat([[Fp, 0],[0, 1]])
      p2 = block_mat([[1, -G],[0, 1]])
      p3 = block_mat([[F, 0],[0, 1]])
      
      p4 = block_mat([[1, 0],[0, -Sp]])
      p5 = block_mat([[1, 0],[-D, 1]])
      p6 = block_mat([[Fp, 0],[0, 1]])

      P = p1*p2*p3*p4*p5*p6
      return P

class Y_1():
   def __str__(self):
      return 'Yosida_1'

   def generate_prec(self, AA, p, q, u, v, P, bcu=None, bcp=None, 
                     u_1=None, dim=3):
      mu = P.mu
      dt = P.dt
      
      [[F, G],
       [D, Q]]  = AA
 
      Fp = AMG(F)
      
      Mp = assemble(p*q*dx)
      Mudt = assemble((1/dt)*inner(u,v)*dx)

      if bcu != None:
         for bc in bcu:
            bc.apply(Mudt)
      if bcp != None:
         for bc in bcp:
            bc.apply(Mp)

      S = D*LumpedInvDiag(Mudt)*G + Mp 
      Sp = AMG(collapse(S))
      
      p1 = block_mat([[Fp, 0],[0, 1]])
      p2 = block_mat([[1, -G],[0, 1]])
      p3 = block_mat([[F, 0],[0, 1]])
      
      p4 = block_mat([[1, 0],[0, -Sp]])
      p5 = block_mat([[1, 0],[-D, 1]])
      p6 = block_mat([[Fp, 0],[0, 1]])

      P = p1*p2*p3*p4*p5*p6
      return P

