#!/usr/bin/env python
from math import log
def atoi(text):
    return int(text) if text.isdigit() else text
def natural_keys(text):
    '''
    alist.sort(key=natural_keys) sorts in human order
    http://nedbatchelder.com/blog/200712/human_sorting.html
    '''
    return [ atoi(c) for c in re.split('(\d+)', text) ]

class Result:
   """
   class for storing and printing simulation results.
   maybe include visualizing, .tex-generating etc. ?
   """
   def __str__(self):
      """ print all the exciting stuff. all of it or just part? 
      one string per simulation.
      """
      output = vars(self)
      string = ''
      for key in output:
         if output[key] != None:
            string += '%s:%s ' % (key, output[key])
      return string

   def take_input(r, problem=None, velements=None, pelements=None,
                  time=None, iterations=None, u=None, p=None,
                  Nt=None, N=None, prec=None, mu=None, h=None,
                  stdev=None, sol=None, cond = None, dt=None, symm=None,
                  rho=None, build_time = None, solve_time=None):
      r.problem      = problem # TODO does this work?
      r.ve           = str(velements)
      r.pe           = str(pelements)
      r.time         = '%.2f' % time if time != None else None
      r.build_time   = "%.2f" % build_time if build_time != None else None
      r.solve_time   = "%.2f" % solve_time if solve_time != None else None
      r.its          = '%d'% iterations
      r.prec         = str(prec)
      r.mu           = mu 
      r.N            = str(N) if N != None else None
      r.sol          = sol
      r.dt           = dt
      r.symm         = symm
      r.Nt           = Nt
      if stdev != None and stdev != 0: 
         r.stdev     = '%.0f' % stdev
      if cond != None: 
         r.cond      = '%.1f' % cond 

